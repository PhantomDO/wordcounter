package org.cnam.nfp121.wordCounter;

import java.io.IOException;

/**
 * @author donno
 *
 */
public class FileWordCounter extends SecureOperations {

	protected Integer nbWord;
	protected String data;
	protected Manipulator manipulator;


	  public FileWordCounter(Manipulator manipulator) {
	    super();
	    this.nbWord = 0;
	    this.data = null;
	    this.manipulator = manipulator;
	  }
	  
	  /**
	   *
	   */
	  public void Display() {
	    System.out.println(data + '\n');
	  }

	  /**
	   * @param word
	   */
	  public void CountNumber(String word) {
	    if (!this.isSecure()) {
	      System.out.println("Non secure.");
	      return;
	    }

	    if (manipulator.Open()) {
	    	data = manipulator.Read();
	    	while(data != null)
	    	{
		    	  nbWord += this.Count(word);
		    	  data = manipulator.Read();
	    	}

	    	manipulator.ResetLineNumber();
	    }

	    this.Display();
	    System.out.println(String.format("%s appears %d times in file.", word, nbWord));

	    // Reset lineNumber et nbWord a 0 pour une passe sur un autre mot
	    if (nbWord > 0) {
	      nbWord = 0;
	    }
	  }

	  /**
	   * @param val
	   * @return
	   */
	  public Integer Count(String val) {
	    Integer count = 0;
	    String input = data;
	    if (input != null) {
	      Integer index = input.indexOf(val);
	      while (index != -1) {
	        count++;
	        input = input.substring(index + 1);
	        index = input.indexOf(val);
	      }
	    } else {
	      System.out.println("data is null");
	    }
	    return count;
	  }

}
