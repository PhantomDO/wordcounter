/**
 *
 */
package org.cnam.nfp121.wordCounter;

/**
 * @author donno
 *
 */
public interface FileUtils {

	public Boolean Open();

	public String Read();

	public Boolean Close();
}
