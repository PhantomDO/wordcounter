/**
 *
 */
package org.cnam.nfp121.wordCounter;

/**
 * @author donno
 *
 */
public interface IManipulator {

	public Boolean Open();

	public String Read();

	public Boolean Close();
}
