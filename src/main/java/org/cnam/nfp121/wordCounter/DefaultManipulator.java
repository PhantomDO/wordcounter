/**
 *
 */
package org.cnam.nfp121.wordCounter;

import java.io.IOException;
import java.net.UnknownServiceException;

/**
 * @author donno
 *
 */
public class DefaultManipulator extends Manipulator {

	public DefaultManipulator(String fileName) {
		super(fileName);
	}

	public DefaultManipulator(String fileName, Boolean defaultOpen, Boolean defaultClose, Boolean defaultRead) {
		super(fileName, defaultOpen, defaultClose, defaultRead);
	}

	@Override
	protected String ImplementRead() throws Exception {
		throw new UnknownServiceException();
	}

	@Override
	protected Boolean ImplementOpen() throws IOException {
		throw new IOException();
	}

	@Override
	protected Boolean ImplementClose() throws IOException {
		throw new IOException();
	}

}
