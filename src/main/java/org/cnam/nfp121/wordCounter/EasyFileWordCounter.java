package org.cnam.nfp121.wordCounter;

import java.io.IOException;

public final class EasyFileWordCounter extends FileWordCounter {

  public EasyFileWordCounter(Manipulator manipulator) {
    super(manipulator);
  }

  public Boolean isSecure() {
    return true;
  }
}
