package org.cnam.nfp121.wordCounter;

import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

public abstract class Manipulator implements IManipulator {

	private Boolean useDefaultRead;
	private Boolean useDefaultOpen;
	private Boolean useDefaultClose;

	protected String dsName;

	protected BufferedReader dataSource;

	protected List<String> fileLine;
	protected Integer lineNumber;
	protected Integer maxLine;

	public Manipulator(String fileName) {
		super();
		this.dsName = fileName;
		this.lineNumber = 0;
		this.maxLine = 0;
		this.UseDefaultOpenCloseRead(false, false, false);
	}

	public Manipulator(String fileName, Boolean defaultOpen, Boolean defaultClose, Boolean defaultRead) {
		super();
		this.dsName = fileName;
		this.lineNumber = 0;
		this.maxLine = 0;
		this.UseDefaultOpenCloseRead(defaultOpen, defaultClose, defaultRead);
	}

	/**
	 * Change the boolean to know if your child want to use the default
	 * implementation
	 *
	 * @param open
	 * @param close
	 * @param read
	 */
	public final void UseDefaultOpenCloseRead(Boolean open, Boolean close, Boolean read) {
		this.useDefaultOpen = open;
		this.useDefaultClose = close;
		this.useDefaultRead = read;
	}

	public void ResetLineNumber() {
		lineNumber = 0;
	}

	// #region READ
	protected abstract String ImplementRead() throws Exception;

	private final String DefaultRead() {
		try {
			if (dataSource != null) {
				String data = null;
				if (lineNumber < maxLine) {
					data = fileLine.get(lineNumber);
					lineNumber++;
					if (data == null) {
						System.out.println(String.format("ReadError: data[%d] is null. \n", lineNumber));
					}
				}
				return data;
			} else {
				System.out.println("ReadError: dataSource is null");
			}
		} catch (Exception e) {
			e.printStackTrace();
		}

		return null;
	}

	public final String Read() {
		String read = null;

		if (useDefaultRead) {
			read = DefaultRead();
		} else {
			try {
				read = ImplementRead();
			} catch (Exception e) {
				e.printStackTrace();
			}
		}

		return read;
	}
	// #endregion

	// #region OPEN
	protected abstract Boolean ImplementOpen() throws IOException;

	private final Boolean DefaultOpen() {
		try {
			if (dsName != null) {
				FileReader fileReader = new FileReader(dsName);
				dataSource = new BufferedReader(fileReader);

				String line = null;
				fileLine = new ArrayList<String>();
				while ((line = dataSource.readLine()) != null) {
					fileLine.add(line);
					lineNumber++;
				}

				maxLine = lineNumber;
				lineNumber = 0;
				return true;
			}
		} catch (Exception e) {
			e.printStackTrace();
		}

		return false;
	}

	public final Boolean Open() {
		Boolean open = null;

		if (useDefaultOpen) {
			open = DefaultOpen();
		} else {
			try {
				open = ImplementOpen();
			} catch (IOException e) {
				e.printStackTrace();
			}
		}

		return open;
	}
	// #endregion

	// #region CLOSE
	protected abstract Boolean ImplementClose() throws IOException;

	private final Boolean DefaultClose() {
		try {
			if (dataSource != null) {
				dataSource.close();
				return true;
			}
		} catch (Exception e) {
			e.printStackTrace();
		}

		return false;
	}

	public final Boolean Close() {
		Boolean close = null;

		if (useDefaultClose) {
			close = DefaultClose();
		} else {
			try {
				close = ImplementClose();
			} catch (IOException e) {
				e.printStackTrace();
			}
		}

		return close;
	}

	// #endregion

}
